package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class DeleteById
 */
@WebServlet("/DeleteByIdServlet")
public class DeleteByIdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * This method delete student by using id
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		int id = 0;
		HttpSession session = request.getSession(false);
		if (session != null) {
		 id = Integer.parseInt(request.getParameter("id"));
		boolean result = new StudentServiceImpl().DeleteById(id);
		if(result) {
			RequestDispatcher rd = request.getRequestDispatcher("DeleteMessage.html");
			rd.forward(request, response);
		}else {
			RequestDispatcher rd = request.getRequestDispatcher("error.jsp");
			rd.forward(request, response);
		}
		}else {
			response.sendRedirect("index.html");
		}
			
	}
}
