package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class SearchById
 */
@WebServlet("/SearchByIdServlet")
public class SearchByIdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = 0;
		response.setContentType("text/HTML");
		PrintWriter out = response.getWriter();		
		
			
			//getting student id from HTML page
			id = Integer.parseInt(request.getParameter("searchid"));
	        StudentService service = new StudentServiceImpl();	
	        
		  //calling findbyId method of service class
		   Student student = service.findById(id);
		    RequestDispatcher rd = request.getRequestDispatcher("searchbyid.jsp");
		  
		//if student present in database then displaying HTML table 
   		if(student != null) {

   			
   	   		request.setAttribute("student", student);
   			rd.forward(request, response);
   		}else {
 			
   			response.sendRedirect("error.html");
   		}
   		//closing printWriter stream
		out.close();
	}

}


