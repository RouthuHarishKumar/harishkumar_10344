package com.sample.service;

import java.util.List;
import com.sample.bean.Student;

public interface StudentService {
	
	/**
	 * This method get student object as parameter and call insert method from DAO
	 * @param student object
	 */
	boolean insertStudent( Student student );
	
	/**
	 * This method search student based on id 
	 * @param student id
	 */
	Student findById(int id);
	
	/**
	 * This Method gets all student from database and shows to user
	 * @return student list
	 */
	List<Student> fetchAllStudents();
	
}
