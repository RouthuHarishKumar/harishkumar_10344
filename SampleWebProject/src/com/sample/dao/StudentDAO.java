package com.sample.dao;

import java.util.List;

import com.sample.bean.Student;

public interface StudentDAO {
	

	/**
	 * This method insert New student record into database
	 * @param student object
	 * @return boolean result
	 */
	boolean createStudent( Student student );
	
	/**
	 * This Method search student in database as per given id
	 * @param student id
	 * @return student object
	 */
	Student searchById(int id);
	

	/**
	 * This method get all student present in database
	 * @return list object
	 */
	List<Student> getAllStudents();
	
}
