<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>
<table>
<tr>
<th>Id </th>
<th>Name </th>
<th> Age </th>
<th>City </th>
<th>State </th>
<th>pincode</th>
</tr>
<c:forEach var ="student" items="${studentList}">
<tr>
<td>${student.id }</td>
<td>${student.name }</td>
<td>${student.age }</td>
<td>${student.address.city }</td>
<td>${student.address.state }</td>
<td>${student.address.pincode }</td>
</tr>
</c:forEach>
</table><br><br>
<div align="center">
<a href="index.html">Back to HomePage</a></div>

</body>
</html>