package com.abc.bean;

import org.springframework.stereotype.Component;
/**
 * 
 *  This Address class is used to generate setters and getter methods for Address credentials.
 * @author HARISH
 *
 */
@Component
public class Address {
	
	private String city;
	private String state;
	
	
	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public void setState(String state) {
		this.state = state;
	}

}
