package com.abc.bean;

import org.springframework.stereotype.Component;
/**
 *  This Employee class is used to generate setters and getter methods for Employee credentials.
 * @author HARISH
 *
 */
@Component
public class Employee {
     
	
	    private int emp_Id;
	    private String empName;
	    private double empSalary;
	    
	    
		public int getEmp_Id() {
			return emp_Id;
		}
		public void setEmp_Id(int emp_Id) {
			this.emp_Id = emp_Id;
		}
		public String getEmpName() {
			return empName;
		}
		public void setEmpName(String empName) {
			this.empName = empName;
		}
		public double getEmpSalary() {
			return empSalary;
		}
		public void setEmpSalary(double empSalary) {
			this.empSalary = empSalary;
		}
	     
	 
	    
	 
	}