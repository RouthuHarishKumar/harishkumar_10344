package com.abc.bean;

import java.util.List;

public class Department {
	
	private int deptId;
    private String deptName;
    private List<Employee> employee;
    private List<String> telephoneNumbers;
 
    public int getDeptId() {
        return deptId;
    }
 
    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }
 
    public String getDeptName() {
        return deptName;
    }
 
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }
 
    public List<Employee> getEmployee() {
        return employee;
    }
 
    public void setEmployee(List<Employee> employee) {
        this.employee = employee;
    }
    public List<String> getTelephoneNumbers() {
        return telephoneNumbers;
    }
 
    public void setTelephoneNumbers(List<String> telephoneNumbers) {
        this.telephoneNumbers = telephoneNumbers;
    }    
 
}
