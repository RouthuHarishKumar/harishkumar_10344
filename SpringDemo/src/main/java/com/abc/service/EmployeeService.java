package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.abc.bean.Employee;
import com.abc.dao.EmployeeDAO;
/**
 * this class is for Employee Service to perform operations byt using DAO
 * @author HARISH
 *
 */
@Service
public class EmployeeService {
	@Autowired
	EmployeeDAO employeeDAO;
	
	//to create an new Employee and getter & setter methods for employeeDao
	public Employee createEmp() {
		Employee employee=employeeDAO.createEmp();
		return employee;
		
	}

	public EmployeeDAO getEmployeeDAO() {
		return employeeDAO;
	}

	public void setEmployeeDAO(EmployeeDAO employeeDAO) {
		this.employeeDAO = employeeDAO;
	}
	
	




}