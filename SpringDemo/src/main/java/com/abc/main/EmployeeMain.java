package com.abc.main;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abc.bean.Department;
import com.abc.bean.Employee;
import com.abc.controller.EmployeeController;
/**
 * This class is the main class for Employee in order to perform by using spring Architecture
 * @author HARISH
 *
 */
public class EmployeeMain {
	
	public static void main(String[] args) {
		
		// open/read the application context file	
	ApplicationContext context = new ClassPathXmlApplicationContext("classpath:/com/abc/resources/context.xml");
	
	//instantiate our spring EmployeeController object from the application context
	EmployeeController controller=(EmployeeController) context.getBean(EmployeeController.class); 
	
	Employee emp = (Employee) context.getBean("employee"); 
   //Employee emp= controller.createEmp();
	
//	  System.out.println("Employee saved:"+emp.getEmp_Id());
//	  System.out.println("Name: "+ emp.getEmpName());
//		System.out.println("City: "+ emp.getAddress().getCity());
//		System.out.println("State: "+ emp.getAddress().getState());
//	  

			Department department = (Department) context.getBean("deptBean");
			System.out.println("DeptId : " + department.getDeptId());
			System.out.println("DeptName : " + department.getDeptName());
			List<String> telephoneNumbers = department.getTelephoneNumbers();
			int i = 1;
			for (String number : telephoneNumbers) {
				System.out.println("Telephone " + i + " :" + number);
				i++;
			}
			System.out.println("===================");
			System.out.println("Employees Under Department No : "+department.getDeptId());
			List<Employee> employees = department.getEmployee();
			for (Employee employee : employees) {
				System.out.println("Employee Id : " + employee.getEmp_Id());
				System.out.println("Empoyee Name : " + employee.getEmpName());
				System.out.println("Employee Salary : " + employee.getEmpSalary());
			}

		}
	

}
