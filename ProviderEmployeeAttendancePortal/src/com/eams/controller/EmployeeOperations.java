package com.eams.controller;

import java.util.List;
import java.util.Scanner;

import com.eams.DAO.AttendanceDAOImpl;
import com.eams.DAO.EmployeeDAOImpl;
import com.eams.bean.Employee;
import com.eams.bean.Leave;
/**
 * This EmployeeOperations is used to define various operations that an Employee can do.
 * @author Batch - G
 *
 */
public class EmployeeOperations {

	static Home home=new Home();
	
	/**
	 * This method is for Employee to perform  operations like view he/she details and applying for leaves 
	 * @param username
	 */
	public void EmployeeOpertaions(String username) {
		
		int choice = 0;
		boolean flag=true;
		while(flag) {
			
		
		try {
		System.out.println();
		System.out.println("1.view Details");
		System.out.println("2.Apply Leaves");
     	System.out.println("3.Logout");
     	//scanner class 
		Scanner sc =  new Scanner(System.in);
		choice=sc.nextInt();
		if(choice == 1 || choice==2 || choice==3 )
		flag = true;
	} catch(Exception e){
		System.out.println("Please enter valid input");
		flag = false;
	}


switch(choice){
case 1:
	//to view employee all details
	EmployeeDAOImpl emp = new EmployeeDAOImpl();
	//employee list is to fetch all details of particular employee
	List<Employee> list=emp.employeeLogin1(username);
	//System.out.println(list);
	for (int i = 0; i< list.size(); i++) {
	System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");
    System.out.printf("%20s %20s %20s %20s %20s %20s %20s ", "Emp_id","Name", "Phone_Number", "Email", "Address","Date_of_Joining", "Salary","Presented Days");
    System.out.println();
	System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");
	 System.out.printf("%20s %20s %20s %20s %20s %20s %20s ", list.get(0).getEmp_id(), list.get(0).getEmp_name(), list.get(0).getPhone_number(), list.get(0).getEmail(), list.get(0).getAddress(),list.get(0).getDate_of_joining(),list.get(0).getSalary(),list.get(0).getPresent_days());
//	System.out.println(list.get(0).getEmail());
	}
	
	break;
	
	
case 2:
	//employee can apply leaves 
	System.out.println("1.Causal leave");
	System.out.println("2.Sick leave");
	Scanner sc =  new Scanner(System.in);
//	System.out.println("i am here");
	int choice1 = sc.nextInt();
//	System.out.println("i am here 2");
	if (choice1 == 1) {
//		System.out.println("i am here3");
		System.out.println("Enter Employee ID");
		int id = sc.nextInt();
		System.out.println("Enter date of leave (YYYY-MM-DD)");
		String  dol = sc.next();
		System.out.println("Enter reason:");
		String  reason = sc.next();
		Leave leave = new Leave();
		leave.setEmp_id(id);
		leave.setDate_of_leave(dol);
		leave.setReason(reason);
//		System.out.println(leave);
		AttendanceDAOImpl cl = new AttendanceDAOImpl();
		cl.ApplyCausalleave(leave);
	}
	else {
		System.out.println("please select valid operation");
		
	}
	if (choice1 == 2) {
		System.out.println("Enter Employee ID");
		int id = sc.nextInt();
		System.out.println("Enter date of leave (YYYY-MM-DD)");
		String  dol = sc.next();
		System.out.println("Enter reason:");
		String  reason = sc.next();
		Leave leave = new Leave();
		leave.setEmp_id(id);
		leave.setDate_of_leave(dol);
		leave.setReason(reason);
//		System.out.println(leave);
		AttendanceDAOImpl cl = new AttendanceDAOImpl();
		cl.ApplySickleave(leave);
	}
	else {
		System.out.println("please select valid operation");	
	}
	
	break;
case 3:
	//logout function
	System.out.println("successfully logged out...!");
	//back to home page
	Home.login();
	break;
		}
		
	}
	}
}


