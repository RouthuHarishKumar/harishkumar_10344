package com.eams.controller;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.eams.DAO.AttendanceDAOImpl;
import com.eams.DAO.EmployeeDAOImpl;
import com.eams.bean.Employee;
import com.eams.bean.Leave;
import com.eams.services.EmployeeServiceImpl;
/**
 * This AdminModule is used to define various operations that an Admin can do.
 * @author Batch - G
 *
 */
public class AdminModule {
	
	static Home home=new Home();
    /**
     * This method is performed by admin to done his operations
     */
	
	@SuppressWarnings("resource")
	public static void operations() {
		//scanner class
		Scanner sc = new Scanner(System.in);
		int Choice=0;
		boolean flag = false;
		while(!flag) {
			try {
				System.out.println("============================================");
				System.out.println("choose");
				System.out.println("1.New Employee Registration");
				System.out.println("2.Update Employee Details");
				System.out.println("3.Delete Employee Details");
				System.out.println("4.View Leave Requests");
				System.out.println("5.Display Employee Details");
				System.out.println("6.Display particular Employee Details");
				System.out.println("7.Display Employee Attendance Details");
				System.out.println("8.Logout");
				System.out.println("============================================");
				
				//scanner class
				Scanner s=new Scanner(System.in);
				Choice = s.nextInt();
				if (Choice == 1 || Choice == 2 || Choice==3 || Choice==4 || Choice==5 || Choice==6 || Choice==7 || Choice==8 ) 
					flag = true;
			}catch(Exception e) {
				System.out.println("Please enter valid input");
				System.out.println("===========================================");
				flag = false;
			}
		}
		switch(Choice) {
		case 1:
			//New Employee Registration
			EmployeeRegistraionImpl user = new EmployeeRegistraionImpl();
			user.employeeRegstrationImpl();
			
			break;
			
			
		case 2:
			//Update Employee Details with Emp_ID
			System.out.println("Enter Employee Id:");
			int update = sc.nextInt();
			System.out.println("Enter New Phone number:");
			String updateNo = sc.next();
			//Employee object creation
			Employee update1 = new Employee();
			update1.setPhone_number(updateNo);
			update1.setEmp_id(update);
			EmployeeServiceImpl updateData = new EmployeeServiceImpl();
			updateData.updateEmployee(update, updateNo);
//			List <Employee> updateList = new EmployeeDAOImpl().updateEmployeeDAO(update, updateNo);
			System.out.println("phone number updated" );
			//back to admin operations
			AdminModule.operations();
			break;
			
			
		case 3:
			//Delete particular Employee Details
			System.out.println("Enter Employee Id:");
			int delete = sc.nextInt();
			EmployeeServiceImpl deleteData = new EmployeeServiceImpl();
			deleteData.deleteEmployee(delete);
			//back to admin operations
			AdminModule.operations();
			break;
			
			
		case 4:
			//View Leave Requests of an Employee
			System.out.println("Enter Employee Id:");
			int emp_id = sc.nextInt();
			List<Leave> leave = new EmployeeServiceImpl().fetchAllLeave(emp_id);
			Iterator<Leave> iterator = leave.iterator();
			System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");
	        System.out.printf("%20s %20s %20s", "Emp_id","Date of Leave", "Reason");
	        System.out.println();
			System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");

			while(iterator.hasNext()) {
				Leave leave1 = (Leave) iterator.next();
	            System.out.format("%20s %20s %20s" ,leave1.getEmp_id(), leave1.getDate_of_leave(), leave1.getReason());
	            System.out.println();
	        
				System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");
		}	
			//back to admin operations
			AdminModule.operations();
			break;
			
			
		case 5:
			//Display All Employee Details
			EmployeeServiceImpl userData = new EmployeeServiceImpl();
			userData.fetchAllEmployee();
			List <Employee> empList = new EmployeeDAOImpl().fetchAllEmployeeDAO();
			Iterator<Employee> itr = empList.iterator();
			
				System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");
		        System.out.printf("%20s %20s %20s %20s %20s %20s ", "Emp_id","Name", "Phone_Number", "Email", "Address","Date_of_Joining");
		        System.out.println();
				System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");

				while(itr.hasNext()) {
					Employee emp = (Employee) itr.next();
		            System.out.format("%20s %20s %20s %20s %20s %20s" ,emp.getEmp_id(),emp.getEmp_name(),emp.getPhone_number(),emp.getEmail(),emp.getAddress(),emp.getDate_of_joining());
		            System.out.println();
		        
					System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");
			}
				//back to admin operations
			AdminModule.operations();
			break;
			
			
		case 6:
			//Display particular Employee Details
			System.out.println("Enter Employee Id:");
			int user1 = sc.nextInt();
			List <Employee> list = new EmployeeDAOImpl().findByIdDAO(user1);
			Iterator<Employee> itre = list.iterator();
			System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");
	        System.out.printf("%20s %20s %20s %20s %20s %20s %20s ", "Emp_id","Name", "Phone_Number", "Email", "Address","Date_of_Joining","salary");
	        System.out.println();
			System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");

			while(itre.hasNext()) {
				Employee emp = itre.next();
	            System.out.format("%20s %20s %20s %20s %20s %20s %20s" ,emp.getEmp_id(),emp.getEmp_name(),emp.getPhone_number(),emp.getEmail(),emp.getAddress(),emp.getDate_of_joining(),emp.getSalary());
	            System.out.println();
	        
				System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");
		}	
			//back to admin operations
			AdminModule.operations();
			break;
			
			
		case 7:
			//Display Employee Attendance Details
			System.out.println("case 6");
			AttendanceDAOImpl details = new AttendanceDAOImpl();
			details.AttendanceDetails();
			//list is to fetch all details of an employee attendance
			List <Employee> attendance = new AttendanceDAOImpl().AttendanceDetails();
			Iterator<Employee> itrator = attendance.iterator();
			
				System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");
		        System.out.printf("%20s %20s", "Emp_id", "Presented_days");
		        System.out.println();
				System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");

				while(itrator.hasNext()) {
					Employee employee = (Employee) itrator.next();
		            System.out.format("%20s %20s" , employee.getEmp_id(), employee.getPresent_days());
		            System.out.println();
					System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");
			}
				//back to admin operations
			AdminModule.operations();
		
			break;
		
		case 8:
			//Logout function
			System.out.println("Logout successfully");
			//back to home page
			Home.login();
			break;
	}
	}

}
