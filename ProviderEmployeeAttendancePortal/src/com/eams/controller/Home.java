package com.eams.controller;


import java.util.Scanner;
import com.eams.bean.Admin;
import com.eams.bean.Employee;
import com.eams.services.AdminServices;
import com.eams.services.AdminServieImp;
import com.eams.services.EmployeeServiceImpl;

/**
 * This is the main class where the users (admin & employee) can select their respective login pages.
 * @author Batch-G
 *
 */
public class Home {
	/**
	 * This is the main method where the users (admin & employee) can select their respective login pages.
	 */
	static AdminServices authentication = new AdminServieImp();
	static Employee employee = new Employee();	
	public   static void main(String[] args) {
		
		System.out.println("====================Welcome to Innominds==============================");
		System.out.println("");
		// This method has portals for different users
		login();
	}
	/**
	 * Using this method where the users (admin & employee) can select their respective login pages.
	 */
	@SuppressWarnings({ "unused", "resource" })
	public  static void login() {
		
		int choice = 0;
		boolean flag = false;
		while(!flag) {
			try {
				System.out.println("============================");
				System.out.println("Select User");
				System.out.println("============================");
				AdminServieImp authenication=new AdminServieImp();
				System.out.println("1) Admin");
				System.out.println("2) Employee");
				System.out.println("-----------------------------");
				//scanner class
				Scanner s=new Scanner(System.in);
				choice=s.nextInt();
				if(choice == 1 || choice==2)
				flag = true;
			} catch(Exception e){
				System.out.println("Please enter valid input");
				flag = false;
			}
		}
		
		switch(choice){
		case 1:
			//This method has two portals where Admin and Employee can access their accounts.
			AdminLogin();
			break;
		case 2:
			EmployeeLogin();
			break;
		}
	}
	/**
	 * Using this method  the Admin can login into his account.
	 */
	
	@SuppressWarnings("resource")
	public  static void AdminLogin() {
		//scanner class
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter username");
		String username=scan.next();
		System.out.println("Enter password");
		String password=scan.next();
		//calling admin object
	    Admin admin = new Admin();
	    //setting the admin credentials
	    admin.setAdmin_username(username);
	    admin.setAdmin_password(password);
		Admin adminDetails=authentication.findByUsername(username);
	
		if(adminDetails!=null) {
			//validating the admin credentials
			if(username.equals(adminDetails.getAdmin_username()) && password.equals(adminDetails.getAdmin_password())) {
				System.out.println("Login Successfully");
				
				AdminModule.operations();
			}
		} else {
			System.out.println("Something Wrong..Pls Try Again!!!");
			//Back to AdminLogin page
			AdminLogin();
			return;
		}		
	}
	/**
	 * This is the Employeelogin method where the Employees can login into their respctive accounts.
	 */
	@SuppressWarnings("resource")
	private static void EmployeeLogin() {
		//scanner class
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter username");
		String username=scan.next();
		System.out.println("Enter password");
		String password=scan.next();
		//Employee object creation
		Employee user = new Employee();
		//setting the emp_name and password
		user.setEmp_name(username);
		user.setPassword(password);
		//calling employeeserviceimpl class object
		EmployeeServiceImpl employeeImp=new EmployeeServiceImpl();
		//EmployeeDAOImpl user= new EmployeeDAOImpl();
		boolean user1 = employeeImp.employeeLogin(username, password);
		if (user1) {
			System.out.println("Login Successful..!");
			EmployeeOperations op= new EmployeeOperations();
			op.EmployeeOpertaions(username);
		}
		else {
			System.out.println("please enter valid login credentials...!");
			//back to EmployeeLogin Page
			EmployeeLogin();
		}
		}
		
		
	}
	      
	


