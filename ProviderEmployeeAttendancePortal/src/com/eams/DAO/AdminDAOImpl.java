package com.eams.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.eams.bean.Admin;
import com.eams.util.DBConnectionUtil;

import sun.security.util.Password;


/**
 * This is implementation class for the AdminDAO interface.
 * @author Batch - G
 *
 */
public class AdminDAOImpl implements AdminDAO {
	
	
	/**
	 * This method is used to create an admin from the database.
	 * @return result
	 * 
	*/
	@Override
	public boolean createAdminDAO(Admin adminDetails) {
		
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		//SQL query to insert new Admin details
		String sql = "insert into admin values( ?, ?)";
		try {
			//getting the connection from Database
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, adminDetails.getAdmin_username());
			ps.setString(2, adminDetails.getAdmin_password());
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;
			} 
		}
		catch (SQLException e){
			e.printStackTrace();
			
		}
		finally {
			try {
				//connection close
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;	
	}
	
	
	/**
	 * this method is used to create an admin from the database.
	 * @return adminDetails
	 */	
	@Override
	public  Admin searchByUsername(String username) {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Admin adminDetails=null;
		//SQL query to search Admin with his/her username
		String sql = "select * from admin where Admin_username = ? ";
		try {
			//getting the database from database
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, username);
			rs = ps.executeQuery();
			
			while(rs.next()) {
				adminDetails= new Admin();
				adminDetails.setAdmin_id(rs.getInt(1));
				adminDetails.setAdmin_username(rs.getString(2));
				adminDetails.setAdmin_password(rs.getString(3));
				adminDetails.setSecurity_ques(rs.getString(4));
			}
		}
		catch (SQLException e){	
			e.printStackTrace();
		}
		finally {
			try {
				//close connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return adminDetails;
	}
	
	
	/**
	 * this method is used to delete an admin from the database.
	 * @return result
	 */
	@Override
	public boolean deleteAdminDAO(String username) {
		boolean result=false;
		Connection con = null;
		PreparedStatement ps = null;
		//sql query to delete a particular Employee with his/her Username
		String sql = "DELETE FROM admin WHERE Admin_username=?";
		try {
			//getting the connection from database
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, username);
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;
			} 
		}
		catch (SQLException e){
			e.printStackTrace();
			
		}
        return result;

	}
	
	
	/**
	 * This method is for adminlogin with his username and {@link Password
	 * @return password
	 * 
	 */
	@Override
	public String AdminLogin(String username, String password ) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		//SQL query for Admin Login 
		String sql = "select Admin_password from admin where Admin_username = ?";
		try {
			// getting connection object and executing query
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, username);

			rs = ps.executeQuery();
			if (rs.next()) {
				password = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
        //return Statement
		return password;
	}

	}
	
	

	
	
