package com.eams.test;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.eams.bean.Admin;
import com.eams.services.AdminServices;
import com.eams.services.AdminServieImp;

public class AdminServiceTest {
	
	AdminServices service = null;
	
	@Before
	public void setUp() {
		service = new AdminServieImp();	
	}
	
	@After
	public void tearDown() {
		
		service = null;
	}
/**
 * searchByUsername of an admin unit testing for positive scenario means values are  existed in database
 */	
@Test
public void searchByUsernamePositive() {
			
	assertNull(service.findByUsername("username"));
	
	}

/**
 * searchByUsername of an admin unit testing for negative scenario means values are  existed in database
 */	
@Test	
public void searchByUsernameNegative() {
		
		assertNull(service.findByUsername("Varun"));
	}



/**
 *deleteAdmin unit testing for positive scenario means values are  existed in database
 */
@Ignore
@Test
public void deleteAdminTestPositive() {
	assertTrue(service.deleteAdmin("Anilkumar"));
	 
}

/**
 *deleteAdmin unit testing for negative scenario means values are  existed in database
 */
@Ignore
@Test
public void deleteAdminTestNegative() {
	assertTrue(service.deleteAdmin("Nileesh"));
	 
}


/**
 * insertAdmin unit testing scenario means values are  existed in database
 */
@Test
public void insertAdminTest() {
	
	Admin admin = new Admin();
	admin.setAdmin_id(101);
	admin.setAdmin_username("Nilesh");
	admin.setAdmin_password("Nilesh@889");
	admin.setSecurity_ques("dull");
	assertFalse(service.insertAdmin(admin));
	
}


@Before
/**
 * adminLogin unit testing for positive scenario means values are  existed in database
 */
@Test
public void AdminLoginTestPositive() {
    
	Admin admin = new Admin();
	admin.setAdmin_username("Harish");
	admin.setAdmin_password("Harish@123");
    assertFalse(service.AdminLogin("Harish", "Harish@889"));
    System.out.println("login successfull");
}
/**
 * employeeLogin unit testing for negative scenario means values are not existed in database
 */
 @Test
  public void AdminLoginTestNegative() {
	 
	 Admin admin = new Admin();
	 admin.setAdmin_username("Nilesh");
	 admin.setAdmin_password("Nilesh@123");
	 assertFalse(service.AdminLogin("Harish", "Nilesh@889"));
	 System.out.println("login  not successfull");
	}	
	
}
