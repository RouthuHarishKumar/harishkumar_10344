package com.eams.test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	AdminServiceTest.class,
	EmployeeServiceTest.class
})
/**
 * 
 * This class is for to test all the admin and employee scenrio's at a time 
 * @author BATCH-G
 *
 */
public class AllTestSuite {

	
}
