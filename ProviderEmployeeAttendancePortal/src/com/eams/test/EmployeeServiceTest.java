package com.eams.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.eams.bean.Employee;
import com.eams.bean.Leave;
import com.eams.services.EmployeeService;
import com.eams.services.EmployeeServiceImpl;

/**
 * this class is for to test EmployeeServices by using JUNIT test
 * @author BATCH-G
 *
 */
public class EmployeeServiceTest {

	EmployeeService service = null;

	@Before
	public void setUp() {
		service = new EmployeeServiceImpl();
	}

	@After
	public void tearDown() {

		service = null;
	}

	/**
	 * findById of an employee unit testing for positive scenario means values are
	 * existed in database
	 */
	@Test
	public void findByIdPositive() {

		List<Employee> list = service.findById(2);

		assertEquals(1, list.size());
	}

	/**
	 * findById of an employee unit testing for negative scenario means values are
	 * existed in database
	 */
	@Test
	public void findByIdNegative() {

		List<Employee> list = service.findById(0);
		assertEquals(0, list.size());
	}

	/**
	 * deleteEmployee unit testing for positive scenario means values are existed in
	 * database
	 */
    @Ignore
	@Test
	public void deleteEmployeePositive() {
		
		assertFalse(service.deleteEmployee(3));
		System.out.println("employee deleted");
	}

	/**
	 * deleteEmployee unit testing for negative scenario means values are existed in
	 * database
	 */

	@Ignore
	@Test
	public void deleteEmployeeNegative() {
		
		assertFalse(service.deleteEmployee(5));
		System.out.println("employee not deleted");
	}

	/**
	 * insertEmployee unit testing scenario means values are existed in database
	 */
	@Test
	public void insertEmployeeTest() {

		Employee employee = new Employee();

		employee.setEmp_id(4);
		employee.setEmp_name("harish");
		employee.setPassword("harish@889");
		employee.setConfirm_password("harish@889");
		employee.setPhone_number("9441494889");
		employee.setEmail("harish889@gmail.com");
		employee.setDate_of_joining("2018-12-11");
		employee.setDepartment_id(1);
		assertFalse(service.insertEmployee(employee));

	}

	@Before
	/**
	 * employeeLogin unit testing for positive scenario means values are existed in
	 * database
	 */
	@Test
	public void employeeLoginTest() {
		
		Employee employee = new Employee();
		employee.setEmp_name("Harish");
		employee.setPassword("Harish@123");
		assertFalse(service.employeeLogin("Harish", "Harish@889"));
		System.out.println("login successfull");
	}

	/**
	 * employeeLogin unit testing for negative scenario means values are not existed
	 * in database
	 */
	@Test
	public void employeeLoginTestNegative() {
		
		Employee employee = new Employee();
		employee.setEmp_name("kumar");
		employee.setPassword("kumar@889");
		assertTrue(service.employeeLogin("kumar", "kumar@889"));
		System.out.println("login  not successfull");
	}

	/**
	 * updateEmployee unit testing for positive scenario means values are existed in
	 * database
	 */
	@Test
	public void updateEmployeePositive() {
		
		assertFalse(service.updateEmployee(4, "7989440812"));
		System.out.println("employee updated Successfully");
	}

	/**
	 * updateEmployee unit testing for negative scenario means values are existed in
	 * database
	 */
	@Test
	public void updateEmployeeNegative() {
		
		assertFalse(service.updateEmployee(5, "9441494889"));
		System.out.println("employee not updated");
	}

	/**
	 * * fetchAllLeave of an employee unit testing scenario means values are existed in 
	 * database
	 */
	@Test
	public void fetchAllLeave(int emp_id) {

		List<Leave> leave = service.fetchAllLeave(emp_id);

		assertFalse(leave.isEmpty());
	}

	/**
	 * * fetchAllEmployee of an employee unit testing scenario means values are existed in 
	 * database
	 */
	@Test
	public void fetchAllEmployee() {

		List<Employee> emp = service.fetchAllEmployee();

		assertFalse(emp.isEmpty());

	}
}
