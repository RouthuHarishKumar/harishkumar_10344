package com.eams.bean;
/**
 * This Employee class is used to generate setters and getter methods for Employee credentials.
 * @author Batch - G
 *
 */
public class Employee {
	
	   private int emp_id;
	   private String emp_name;
	   private String password;
	   private String confirm_password;
	   private String phone_number;
	   private String email;
	   private String address;
	   private String date_of_joining;
	   private int Department_id;
	   private double salary;
	   
	   
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	private int present_days;
	   
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
   public int getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(int emp_id) {
		this.emp_id = emp_id;
	}
	public String getConfirm_password() {
		return confirm_password;
	}
	public String getEmp_name() {
		return emp_name;
	}
	public void setEmp_name(String emp_name) {
		this.emp_name = emp_name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setConfirm_password(String confirm_password) {
//		{
//			if(Employee.password(Employee.getPassword(),confirm_password))
//			{
//				this.confirm_password = confirm_password;
//			}
//			else
//			{
//				System.out.println("password and confirm password doesn't matches....plz re-enter.....");
//			}
		this.confirm_password = confirm_password;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDate_of_joining() {
		return date_of_joining;
	}
	public void setDate_of_joining(String date_of_joining) {
		this.date_of_joining = date_of_joining;
	}
	public int getDepartment_id() {
		return Department_id;
	}
	public void setDepartment_id(int department_id) {
		Department_id = department_id;
	}
	public int getPresent_days() {
		return present_days;
	}
	public void setPresent_days(int present_days) {
		this.present_days = present_days;
	}
	
}
