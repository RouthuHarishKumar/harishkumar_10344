package com.abc.dao;

import java.util.List;

import com.abc.entities.Account;

/**
 * This is the AccountDao interface where various Account Abstract methods are created. 
 * @author HARISH
 *
 */
public interface AccountDao {

	Account getAccountByAccNo(int accno);

	int createAccount(Account account);

	List<Account> getAllAccounts();

	void updateAccount(Account account);

	void deleteAccount(int accno);

}
