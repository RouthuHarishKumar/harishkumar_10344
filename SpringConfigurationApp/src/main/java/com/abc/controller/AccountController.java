package com.abc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.abc.entities.Account;
import com.abc.service.AccountService;
/**
 * This AccountController is used to performs different operation of account
 * @author HARISH
 *
 */
@RestController
public class AccountController {

	@Autowired
	private AccountService accountService;

	@PostMapping("/account/create")
	public ResponseEntity<?> createNewAccount(@RequestBody Account account) {
		int id = accountService.saveAccount(account);

		return new ResponseEntity(HttpStatus.CREATED);
	}

	@GetMapping("/account/{id}")
	public ResponseEntity<Account> get(@PathVariable("id") int id) {
		Account account = accountService.getAccountByID(id);
		if (account == null) {

			return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Account>(account, HttpStatus.OK);
	}

	@GetMapping("/account")
	public ResponseEntity<List<Account>> list() {
		List<Account> accounts = accountService.getAllAccountList();
		if (accounts.isEmpty()) {
			return new ResponseEntity<List<Account>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Account>>(accounts, HttpStatus.OK);
	}

	@PutMapping("/account/{id}")
	public ResponseEntity<?> update(@PathVariable("id") int accno, @RequestBody Account account) {
		Account currentAccount = accountService.getAccountByID(accno);

		if (currentAccount == null) {
			return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
		}

		currentAccount.setAccno(account.getAccno());
		currentAccount.setName(account.getName());
		currentAccount.setBalance(account.getBalance());
		accountService.update(currentAccount);
		return new ResponseEntity<Account>(currentAccount, HttpStatus.OK);
	}

	@DeleteMapping("/account/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int accno) {

		Account account = accountService.getAccountByID(accno);
		if (account == null) {

			return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
		}
		accountService.delete(accno);
		return new ResponseEntity<Account>(HttpStatus.NO_CONTENT);

	}
}
