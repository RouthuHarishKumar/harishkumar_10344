package com.abc.service;

import java.util.List;

import com.abc.entities.Account;

public interface AccountService {

//	Account searchByAccNo(int accNo);
//    boolean insertAccount(Account account);
//    boolean deleteAccount(int accNo);
//    List<Account> getAllAccounts();
//    String withdraw(int accNo,double amount);
//	String deposite(int accNo,double amount);

	Account getAccountByID(int accno);

	int saveAccount(Account account);

	List<Account> getAllAccountList();

	void update(Account account);

	void delete(int accno);
}
