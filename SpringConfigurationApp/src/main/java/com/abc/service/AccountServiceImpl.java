package com.abc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.abc.dao.AccountDao;
import com.abc.entities.Account;

@Service
public class AccountServiceImpl implements AccountService{

	@Autowired
	private AccountDao accountDao;
    
	@Transactional
	@Override
	public Account getAccountByID(int accno) {
		// TODO Auto-generated method stub
		return accountDao.getAccountByAccNo(accno);
	}
    
	@Transactional
	@Override
	public int saveAccount(Account account) {
		// TODO Auto-generated method stub
		return accountDao.createAccount(account);
	}
    
	@Transactional
	@Override
	public List<Account> getAllAccountList() {
		// TODO Auto-generated method stub
		List<Account> list = accountDao.getAllAccounts();
		return list;
	}
    
	@Transactional
	@Override
	public void update(Account account) {
		// TODO Auto-generated method stub
		accountDao.updateAccount(account);
		
	}
    
	@Transactional
	@Override
	public void delete(int accno) {
		// TODO Auto-generated method stub
		accountDao.deleteAccount(accno);;
		
	}

}
