<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Search_By_ID</title>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>
<table>
<tr>
<th>Id </th>
<th>Name </th>
<th> Age </th>
<th>City </th>
<th>State </th>
<th>pincode</th>
</tr>
<jsp:useBean id="student" class = "com.sample.bean.Student" scope="request"></jsp:useBean>
<tr>
<th><jsp:getProperty name="student" property="id"/></th>
<th><jsp:getProperty name="student" property="name"/></th>
<th><jsp:getProperty name="student" property="age"/></th>

<th>${student.address.city }</th>
<th>${student.address.state }</th>
<th>${student.address.pincode }</th>
</tr>
</table>
</body>
</html>