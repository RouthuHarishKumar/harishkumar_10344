package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;
import com.sample.util.JsonConverter;

/**
 * Servlet implementation class DisplayAllStudents
 */
@WebServlet("/DisplayAllStudents")
public class DisplayAllStudents extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	/**
	 * This method get student list and display all student records
	 */
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   	
   		//We set the content type of the response object to application/json.
   		response.setContentType("application/json;charset=UTF-8");

   		PrintWriter pw =response.getWriter(); 
   		StudentService service = new StudentServiceImpl();
   		
   		
   		
   		//From the StudentService, we get the list of Students.
   		List<Student> studentList = service.fetchAllStudents();
   		

   		
   		//We convert the list of Students into JSON string with JsonConverter.
   		JsonConverter jsonConverter = new JsonConverter();
   		String result = jsonConverter.convertToJson(studentList);
   		pw.println(result);
   		pw.close();
   	}
   	
}
