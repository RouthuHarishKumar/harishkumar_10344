package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;
import com.sample.util.JsonConverter;

/**
 * Servlet implementation class SearchById
 */
@WebServlet("/SearchByIdServlet")
public class SearchByIdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = 0;
		
		//We set the content type of the response object to application/json.
		response.setContentType("application/json;charset=UTF-8");

   		PrintWriter pw =response.getWriter(); 
			
			//getting student id from HTML page
			id = Integer.parseInt(request.getParameter("searchid"));
	        StudentService service = new StudentServiceImpl();	
	        
		    //calling findbyId method of service class
		     Student student = service.findById(id);
		     
		   //From the StudentService, we get the list of Students.
		     List<Student> studentList=new ArrayList<>();
		     studentList.add(student);
		    
	       //We convert the list of Students into JSON string with JsonConverter.
		    JsonConverter jsonConverter = new JsonConverter();
	   		String result = jsonConverter.convertToJson(studentList);
	   		pw.println(result);
	   		pw.close();
		  
	
	}

}


