-- MySQL dump 10.13  Distrib 5.7.24, for Win64 (x86_64)
--
-- Host: localhost    Database: eams
-- ------------------------------------------------------
-- Server version	5.7.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `Admin_id` int(11) NOT NULL,
  `Admin_username` varchar(15) DEFAULT NULL,
  `Admin_password` varchar(15) DEFAULT NULL,
  `Security_ques` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (10344,'Harish','Harish@123','Rabbit'),(10359,'Mounika','Mounika@123','cat'),(10379,'Anilkumar','Anil@123','Dog');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attendance_data`
--

DROP TABLE IF EXISTS `attendance_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance_data` (
  `emp_id` int(11) DEFAULT NULL,
  `Date` varchar(15) DEFAULT NULL,
  KEY `emp_id` (`emp_id`),
  CONSTRAINT `attendance_data_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendance_data`
--

LOCK TABLES `attendance_data` WRITE;
/*!40000 ALTER TABLE `attendance_data` DISABLE KEYS */;
INSERT INTO `attendance_data` VALUES (3,'2019-01-04'),(3,'2019-01-04'),(1,'2019-01-04'),(3,'2019-01-05'),(3,'2019-01-05'),(3,'2019-01-05'),(3,'2019-01-05'),(3,'2019-01-05'),(3,'2019-01-05'),(3,'2019-01-05'),(3,'2019-01-05'),(1,'2019-01-05'),(3,'2019-01-05'),(3,'2019-01-05');
/*!40000 ALTER TABLE `attendance_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `causal_leaves`
--

DROP TABLE IF EXISTS `causal_leaves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `causal_leaves` (
  `emp_id` int(11) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL,
  KEY `emp_id` (`emp_id`),
  CONSTRAINT `causal_leaves_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `causal_leaves`
--

LOCK TABLES `causal_leaves` WRITE;
/*!40000 ALTER TABLE `causal_leaves` DISABLE KEYS */;
INSERT INTO `causal_leaves` VALUES (1,'2019-01-06','fever'),(3,'2019-01-06','gng to home'),(2,'2019-01-07','marriage');
/*!40000 ALTER TABLE `causal_leaves` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `Department_id` int(11) NOT NULL,
  `Department_name` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`Department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (1,'HR'),(2,'UI'),(3,'Java');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(25) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `confirm_password` varchar(20) DEFAULT NULL,
  `phone_number` varchar(11) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `date_of_joining` varchar(50) DEFAULT NULL,
  `Department_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`emp_id`),
  KEY `Department_id` (`Department_id`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`Department_id`) REFERENCES `departments` (`Department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'Nilesh','abcd@123','abcd@123','8555802533','employee@gmaiil.com','UP','2018-12-31',1),(2,'Mounika','Mounika@123','Mounika@123','7894561230','mounika@gmail.com','Hyd','2018-12-31',3),(3,'Anilkumar','Anil@123','Anil2123','8555802533','employee@gmaiil.com','UP','2018-12-31',2),(4,'abc','abcd@123','abcd@123','7894561233','employee@gmaiil.com','UP','2018-12-31',1),(5,'Avinash','Ani@123','Avi@123','8978160259','employee@gmaiil.com','UP','2018-12-31',2),(6,'harish','Harish@12','Harish@12','7894561233','employee@gmaiil.com','UP','2018-12-31',2),(7,'Anusha','Anusha@123','Anusha@123','1234567890','employee@gmaiil.com','UP','2018-12-31',2);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sick_leaves`
--

DROP TABLE IF EXISTS `sick_leaves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sick_leaves` (
  `emp_id` int(11) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL,
  KEY `emp_id` (`emp_id`),
  CONSTRAINT `sick_leaves_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sick_leaves`
--

LOCK TABLES `sick_leaves` WRITE;
/*!40000 ALTER TABLE `sick_leaves` DISABLE KEYS */;
INSERT INTO `sick_leaves` VALUES (3,'2019-01-15','asdiuah'),(3,'2019-01-07','fdsffg'),(3,'2019-01-10','sufferuifrgt');
/*!40000 ALTER TABLE `sick_leaves` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-21 12:28:35
