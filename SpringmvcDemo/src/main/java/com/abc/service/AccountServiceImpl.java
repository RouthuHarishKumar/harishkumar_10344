package com.abc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.abc.dao.AccountDAO;
import com.abc.hibernate.entities.Account;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountDAO accountDAO;
    
	@Transactional
	@Override
	public Account searchByAccNo(int accNo) {
		
		return accountDAO.getByAccNo(accNo);
	}

	@Transactional
	@Override
	public boolean insertAccount(Account account) {
		boolean insert = accountDAO.insertAccount(account);
		return insert;
	}

	@Transactional
	@Override
	public boolean deleteAccount(int accNo) {
		
		return accountDAO.deleteAccount(accNo);
	}

	@Transactional
	@Override
	public List<Account> getAllAccounts() {
		List<Account> list = accountDAO.getAllAccounts();
		return list;
	}


	@Transactional
	@Override
	public boolean updateAccount(Account account) {
		return accountDAO.updateAccount(account);

	}

	
}