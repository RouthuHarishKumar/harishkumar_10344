package com.abc.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.bean.Employee;
import com.abc.dao.EmployeeDAO;

@Service
public class EmployeeService {
	
	@Autowired
    EmployeeDAO dao;
     
    public List<Employee> getAllEmployees()
    {
        return dao.getAllEmployees();
    }

	public EmployeeDAO getDao() {
		return dao;
	}

	public void setDao(EmployeeDAO dao) {
		this.dao = dao;
	}

}
