package com.abc.service;

import java.util.List;

import com.abc.hibernate.entities.Account;

public interface AccountService {
   
	Account searchByAccNo(int accNo);
    boolean insertAccount(Account account);
    boolean deleteAccount(int accNo);
    List<Account> getAllAccounts();
    boolean updateAccount(Account account);
	
	
}
