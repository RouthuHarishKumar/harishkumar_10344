package com.abc.dao;

import java.util.List;

import com.abc.hibernate.entities.Account;

public interface AccountDAO {
   
	
	Account getByAccNo(int accNo);
	 boolean insertAccount(Account account);
	 boolean deleteAccount(int accNo);
	 List<Account> getAllAccounts();
	 boolean updateAccount(Account account);
	 
	 
}


