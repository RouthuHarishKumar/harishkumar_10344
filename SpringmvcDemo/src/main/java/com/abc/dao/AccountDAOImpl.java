package com.abc.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.hibernate.entities.Account;

@Repository
public class AccountDAOImpl implements AccountDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
    
	@Override
	public Account getByAccNo(int accNo) {

		Session session = sessionFactory.getCurrentSession();
		Account account = session.get(Account.class, accNo);
		return account;
	}
    @Override
	public boolean insertAccount(Account account) {

		Session session = sessionFactory.getCurrentSession();
		int i = (int) session.save(account);
		boolean result = false;
		if (i > 0) {
			result = true;
		}
		return result;
	}

	@Override
	public boolean deleteAccount(int accNo) {
		
		Session session = sessionFactory.getCurrentSession();	
		Account account = session.get(Account.class, accNo);
		session.delete(account);
		return true;
	}

	@Override
	public List<Account> getAllAccounts() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Account");
        List<Account> list = query.getResultList();
		return list;
	}
    
    @Override
	public boolean updateAccount(Account account) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(account);
		return true;
	}
	
}