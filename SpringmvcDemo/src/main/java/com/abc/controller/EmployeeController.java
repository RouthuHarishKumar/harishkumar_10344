package com.abc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.abc.bean.Employee;
import com.abc.service.EmployeeService;

@Controller
@RequestMapping("/employee-module")
public class EmployeeController {
	@Autowired
	EmployeeService employee;

	@GetMapping("/getAllEmployees")
	public ModelAndView getAllEmployees() {
		
		List<Employee> employees = employee.getAllEmployees();
		ModelAndView mav = new ModelAndView("employeesListDisplay", "emp", employees);
		
		// model.addAttribute("employees", employee.getAllEmployees());
		return mav;
	}

	public EmployeeService getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeService employee) {
		this.employee = employee;
	}
}
