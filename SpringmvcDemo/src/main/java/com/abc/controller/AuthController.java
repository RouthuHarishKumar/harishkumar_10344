package com.abc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.abc.bean.Employee;


@Controller
public class AuthController {
	
	@GetMapping("loginform")
	public String getLoginForm() {
		return "login";
	}
	
	@GetMapping("registerform")
	public String getRegisterForm() {
		return "registration";
	}

	@PostMapping("/login")
	public String doLogin(@RequestParam("email") String email,@RequestParam("pwd") String pwd,ModelMap map) {
		
		if(pwd.equals("abc@123")) {
			map.addAttribute("myemail", email);
			return "welcome H@ri$H";
		}
		else {
			return "login failed..!!";
		}
		
	}
	
	@PostMapping("/register")
	public String doRegister(@ModelAttribute Employee user,ModelMap map) {
		
		if(user.getPassword().length() < 3) {
		   return "registration failed";	
		}
		else {
//			map.addAttribute("userDetails", user);
			return "registration success";
		}
		
		
	}
	
	
}
