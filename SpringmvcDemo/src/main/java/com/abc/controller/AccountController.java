package com.abc.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.abc.hibernate.entities.Account;
import com.abc.service.AccountService;

@Controller
public class AccountController {

	@Autowired
	private AccountService accountService;

	
	@RequestMapping("/search/{id}")
	public String searchByAccount(@PathVariable("id") int accno, ModelMap map) {
		Account account = accountService.searchByAccNo(accno);
		map.addAttribute("account", account);
		return "searchbyaccountNo";	
	}
	
	@GetMapping("/insert")
	public String createAccount(@ModelAttribute Account account, ModelMap map) {
	
		boolean result = accountService.insertAccount(account);
		if(result) {
			return "success";
		}else {
			return "failure";
		}
			
	}
	@RequestMapping("/delete/{id}")
	public String deleteAccount(@PathVariable("id") int accno, ModelMap map) {
		boolean result = accountService.deleteAccount(accno);
		if(result) {
			return "success";
		}else {
			return "failure";
		}
	}
	
	@RequestMapping("/displayaccounts")
	public ModelAndView displayAllAccount() {
	
		List<Account> list = accountService.getAllAccounts();
		System.out.println(list);
		ModelAndView mav = null;
		if(list.size() > 0) {
			 mav = new ModelAndView("display_all_accounts","accountList",list);
		}
		return mav;
	}
	
	
	@RequestMapping("/updateAccount")
	public String updateAccount( @ModelAttribute Account account,ModelMap map) {
		boolean result =accountService.updateAccount(account);
		if(result) {
			return "update-Account";
		}else {
			return "update-failure";
		}
		
	}

	
}
