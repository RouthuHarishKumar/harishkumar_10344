<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h3>Accounts</h3>
	<hr>
	<table border="1">
		<tr>
			<td>Account Number</td>
			<td>Account Holder Name</td>
			<td>Account Balance</td>


		</tr>
		<c:forEach items="${accountList}" var="account">

			<tr>
				<td><c:out value="${account.getAccno()}" /></td>
				<td><c:out value="${account.getName()}" /></td>
				<td><c:out value="${account.getBalance()}" /></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>